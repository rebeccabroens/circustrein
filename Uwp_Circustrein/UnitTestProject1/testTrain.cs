﻿using System;
using System.Collections.Generic;
using Circustrein.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace UnitTestProject1
{
    [TestClass]
    public class testTrain
    {
        private Train train;
        private List<Animal> animals;

        [TestInitialize]
        public void Setup()
        {
            animals = new List<Animal>();

            //Add animals
            //One carnivore of each size
            //Three big and medium herbivores
            //Two small herbivores

            animals.Add(new Animal() { type = typeAnimal.carnivore, size = sizeAnimal.big });
            animals.Add(new Animal() { type = typeAnimal.carnivore, size = sizeAnimal.medium });
            animals.Add(new Animal() { type = typeAnimal.carnivore, size = sizeAnimal.small });

            animals.Add(new Animal() { type = typeAnimal.herbivore, size = sizeAnimal.big });
            animals.Add(new Animal() { type = typeAnimal.herbivore, size = sizeAnimal.big });
            animals.Add(new Animal() { type = typeAnimal.herbivore, size = sizeAnimal.big });
            animals.Add(new Animal() { type = typeAnimal.herbivore, size = sizeAnimal.medium });
            animals.Add(new Animal() { type = typeAnimal.herbivore, size = sizeAnimal.medium });
            animals.Add(new Animal() { type = typeAnimal.herbivore, size = sizeAnimal.medium });
            animals.Add(new Animal() { type = typeAnimal.herbivore, size = sizeAnimal.small });
            animals.Add(new Animal() { type = typeAnimal.herbivore, size = sizeAnimal.small });

            train = new Train(animals);
        }

        //Test if the expected count of wagons is the same as the sorted list
        [TestMethod]
        public void TestSortedCount()
        {
            var result = train.GetSortedAnimalsInWagons();

            Assert.AreEqual(5, result.Count);
        }
    }
}
