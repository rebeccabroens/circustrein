﻿using System.Collections.Generic;
using System.Linq;

namespace Circustrein.Classes
{
    public class Train
    {
        private List<Wagon> wagons   = new List<Wagon>();
        private List<Animal> animals = new List<Animal>();

        public Train(List<Animal> animals)
        {
            //Split the animals and then add them to the list
            this.animals.AddRange(splitOrderedList(typeAnimal.carnivore, animals));
            this.animals.AddRange(splitOrderedList(typeAnimal.herbivore, animals));
        }

        //Get a sorted wagon list
        public List<Wagon> GetSortedAnimalsInWagons()
        {
            //Whilst there are herbivores left, keep sorting them
            while (animalsLeft())
            {
                sortAnimal();

                //If there are still herbivores left after the sorting, make a new wagon
                if (animalsLeft())
                {
                    Wagon wagon = new Wagon();
                    wagons.Add(wagon);
                }
            }

            return wagons;
        }

        //Split the list of animals
        private List<Animal> splitOrderedList(typeAnimal type, List<Animal> listToSplit)
        {
            List<Animal> resultList = new List<Animal>();

            foreach (Animal animal in listToSplit)
            {
                //If the animal type is equal to the given type that add it to the list
                if (animal.type == type)
                {
                    resultList.Add(animal);
                }
            }

            return resultList.OrderByDescending(x => x.size).ToList();
        }

        //Check if there are any animals that aren't in a wagon yet in the list
        private bool animalsLeft()
        {
            bool left = false;

            foreach (Animal animal in animals)
            {   
                //If the animal is not in a wagon yet, set left to true
                if (!animal.inWagon)
                {
                    left = true;
                }
            }

            return left;
        }

        //Sort all animals into current wagons
        private void sortAnimal()
        {
            foreach (Wagon wagon in wagons)
            {
                foreach (Animal animal in animals)
                {
                    //If the animal fits, feels safe and isnt in a wagon already then add the animal
                    if (wagon.canAddAnimal(animal) && !animal.inWagon)
                    {
                        wagon.addAnimal(animal);
                    }
                }
            }
        }

    }
}
